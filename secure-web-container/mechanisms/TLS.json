{
  "security_mechanism_id": "TLS",
  "security_mechanism_name": "tls",
  "sm_description": "This mechanism offers transport layer security by encrypting and applying different HTTP features to enforce a reliable and secure HTTP transport protocol.",
  "security_capabilities": [
    "TLS"
  ],
  "enforceable_metrics": [
    "tls_crypto_strength_m3",
    "forward_secrecy_m4",
    "hsts_m5",
    "https_redirect_m6",
    "secure_cookies_m7",
    "ceritificate_pinning_m10"
  ],
  "monitorable_metrics": [
    "tls_crypto_strength_m3",
    "forward_secrecy_m4",
    "hsts_m5",
    "https_redirect_m6",
    "secure_cookies_m7",
    "ceritificate_pinning_m10",
    "specs:terminator_availability:TLS_MSR7",
    "specs:endpoint_availability:TLS_MSR8"
  ],
  "measurements": [
    {
      "msr_id": "tls_crypto_strength_level_msr1",
      "msr_description": "Encryption ciphers cryptographic strength level.",
      "frequency": "10min",
      "metrics": [
        "tls_crypto_strength_m3"
      ],
      "monitoring_event": {
        "event_id": "tls_crypto_strength_too_low_tls_e1",
        "event_description": "Detected cryptographic strength level is too low.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "lt",
          "threshold": "metric:tls_crypto_strength_m3"
        }
      }
    },
    {
      "msr_id": "tls_forward_secrecy_msr2",
      "msr_description": "Existence of the Forward Secrecy feature.",
      "frequency": "10min",
      "metrics": [
        "forward_secrecy_m4"
      ],
      "monitoring_event": {
        "event_id": "tls_forward_secrecy_misconfigured_tls_e2",
        "event_description": "The Forward Secrecy is misconfigured.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "neq",
          "threshold": "metric:forward_secrecy_m4"
        }
      }
    },
    {
      "msr_id": "tls_hsts_msr3",
      "msr_description": "Existence of the HTTP Strict Transport Security feature.",
      "frequency": "10min",
      "metrics": [
        "hsts_m5"
      ],
      "monitoring_event": {
        "event_id": "tls_hsts_misconfigured_tls_e3",
        "event_description": "The HTTP Strict Transport Security is misconfigured.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "neq",
          "threshold": "metric:hsts_m5"
        }
      }
    },
    {
      "msr_id": "tls_http_to_https_redirect_msr4",
      "msr_description": "Existence of the HTTP to HTTPS Redirection feature.",
      "frequency": "10min",
      "metrics": [
        "https_redirect_m6"
      ],
      "monitoring_event": {
        "event_id": "tls_http_to_https_misconfigured_tls_e4",
        "event_description": "The HTTP to HTTPS Redirection is misconfigured.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "neq",
          "threshold": "metric:https_redirect_m6"
        }
      }
    },
    {
      "msr_id": "tls_force_secure_cookies_msr5",
      "msr_description": "Secure cookies enforcement.",
      "frequency": "10min",
      "metrics": [
        "secure_cookies_m7"
      ],
      "monitoring_event": {
        "event_id": "tls_secure_cookies_misconfigured_tls_e5",
        "event_description": "The Secure Cookies is misconfigured.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "neq",
          "threshold": "metric:secure_cookies_m7"
        }
      }
    },
    {
      "msr_id": "tls_certificate_pinning_msr6",
      "msr_description": "Secure pinning feature is enabled",
      "frequency": "10min",
      "metrics": [
        "ceritificate_pinning_m10"
      ],
      "monitoring_event": {
        "event_id": "tls_certificate_pinning_misconfigured_tls_e6",
        "event_description": "The Certificate Pinning is misconfigured.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "neq",
          "threshold": "metric:ceritificate_pinning_m10"
        }
      }
    },
    {
      "msr_id": "tls_terminator_availability_msr7",
      "msr_description": "Availability of the TLS Terminator.",
      "frequency": "10min",
      "metrics": [
        "tls_crypto_strength_m3",
        "forward_secrecy_m4",
        "hsts_m5",
        "https_redirect_m6",
        "secure_cookies_m7",
        "ceritificate_pinning_m10"
      ],
      "monitoring_event": {
        "event_id": "tls_terminator_unavailable_tls_e7",
        "event_description": "The TLS Terminator is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "no"
        }
      }
    },
    {
      "msr_id": "tls_endpoint_availability_msr8",
      "msr_description": "Availability of the TLS Endpoint.",
      "frequency": "10min",
      "metrics": [
        "tls_crypto_strength_m3",
        "forward_secrecy_m4",
        "hsts_m5",
        "https_redirect_m6",
        "secure_cookies_m7",
        "ceritificate_pinning_m10"
      ],
      "monitoring_event": {
        "event_id": "tls_endpoint_unavailable_tls_e8",
        "event_description": "The TLS endpoint is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "no"
        }
      }
    }
  ],
  "metadata": {
    "components": [
      {
        "component_name": "tls_prober",
        "component_type": "prober",
        "recipe": "tls_r1",
        "cookbook": "TLS",
        "implementation_step": 3,
        "pool_seq_num": 1,
        "vm_requirement": {
          "hardware": "m3.medium",
          "usage": "100",
          "acquire_public_ip": "true",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "0.0.0.0/0"
              ],
              "source_nodes": [],
              "interface": "public",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [
                "0.0.0.0/0"
              ],
              "destination_nodes": [],
              "interface": "public,private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      },
      {
        "component_name": "tls_terminator",
        "component_type": "terminator",
        "recipe": "tls_r2",
        "cookbook": "TLS",
        "implementation_step": 0,
        "pool_seq_num": 1,
        "vm_requirement": {
          "hardware": "m3.medium",
          "usage": "100",
          "acquire_public_ip": "true",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "0.0.0.0/0"
              ],
              "source_nodes": [],
              "interface": "public",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [
                "0.0.0.0/0"
              ],
              "destination_nodes": [],
              "interface": "public,private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      },
      {
        "component_name": "tls_terminator_configurator",
        "component_type": "terminator_configurator",
        "recipe": "tls_r3",
        "cookbook": "TLS",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "vm_requirement": {
          "hardware": "m3.medium",
          "usage": "100",
          "acquire_public_ip": "true",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "0.0.0.0/0"
              ],
              "source_nodes": [],
              "interface": "public",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [
                "0.0.0.0/0"
              ],
              "destination_nodes": [],
              "interface": "public,private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      },
      {
        "component_name": "tls_terminator_controller",
        "component_type": "terminator_controller",
        "recipe": "tls_r4",
        "cookbook": "TLS",
        "implementation_step": 2,
        "pool_seq_num": 1,
        "vm_requirement": {
          "hardware": "m3.medium",
          "usage": "100",
          "acquire_public_ip": "true",
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "0.0.0.0/0"
              ],
              "source_nodes": [],
              "interface": "public",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "22",
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [
                "0.0.0.0/0"
              ],
              "destination_nodes": [],
              "interface": "public,private:1",
              "proto": [
                "TCP"
              ],
              "port_list": [
                "*"
              ]
            }
          }
        }
      }
    ],
    "constraints": []
  },
  "remediation": {
    "remediation_actions": [
      {
        "name": "tls_a1",
        "action_description": "Reconfigure TLS crypto strength to M3_value and check if TLS_crypto_strength has the initial M3_value.",
        "recipes": [
          "tls_r5",
          "tls_r6"
        ]
      },
      {
        "name": "tls_a2",
        "action_description": "Restart TLS Terminator and check if it is available.",
        "recipes": [
          "tls_r7",
          "tls_r8"
        ]
      },
      {
        "name": "tls_a3",
        "action_description": "Check if TLS_crypto_strength is >= M3_value.",
        "recipes": [
          "tls_r6"
        ]
      },
      {
        "name": "tls_a4",
        "action_description": "Reconfigure TLS forward secrecy to M4_value and check if TLS_forward_secrecy has the initial M4_value.",
        "recipes": [
          "tls_r9",
          "tls_r10"
        ]
      },
      {
        "name": "tls_a5",
        "action_description": "Check if TLS_forward_secrecy has the initial M4_value.",
        "recipes": [
          "tls_r10"
        ]
      },
      {
        "name": "tls_a6",
        "action_description": "Reconfigure TLS HSTS to M5_value and check if TLS_hsts has the initial M5_value.",
        "recipes": [
          "tls_r11",
          "tls_r12"
        ]
      },
      {
        "name": "tls_a7",
        "action_description": "Check if TLS_hsts has the initial M5_value.",
        "recipes": [
          "tls_r12"
        ]
      },
      {
        "name": "tls_a8",
        "action_description": "Reconfigure TLS HTTP2HTTPS to M6_value and check if TLS_https_to_https_redirect has the initial M6_value.",
        "recipes": [
          "tls_r13",
          "tls_r14"
        ]
      },
      {
        "name": "tls_a9",
        "action_description": "Check if TLS_http_t_https_redirect has the initial M6_value.",
        "recipes": [
          "tls_r14"
        ]
      },
      {
        "name": "tls_a10",
        "action_description": "Reconfigure TLS FSC to M7_value and check if TLS_force_secure_cookies has the initial M7_value.",
        "recipes": [
          "tls_r15",
          "tls_r16"
        ]
      },
      {
        "name": "tls_a11",
        "action_description": "Check if TLS_force_secure_cookies has the initial M7_value.",
        "recipes": [
          "tls_r16"
        ]
      },
      {
        "name": "tls_a12",
        "action_description": "Reconfigure TLS CP to M10_value and check if TLS_certificate_pinning has the initial M10_value.",
        "recipes": [
          "tls_r17",
          "tls_r18"
        ]
      },
      {
        "name": "tls_a13",
        "action_description": "Check if TLS_certificate_pinning has the initial M10_value.",
        "recipes": [
          "tls_r18"
        ]
      },
      {
        "name": "tls_a14",
        "action_description": "Request, to an external service, TLS Endpoint restart and check if the TLS Endpoint is available.",
        "recipes": [
          "tls_r19",
          "tls_r20"
        ]
      }
    ],
    "remediation_flow": [
      {
        "name": "tls_crypto_strength_too_low_tls_e1",
        "action_id": "tls_a1",
        "yes_action": "observe",
        "no_action": {
          "action_id": "tls_a2",
          "yes_action": {
            "action_id": "tls_a3",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "tls_forward_secrecy_misconfigured_tls_e2",
        "action_id": "tls_a4",
        "yes_action": "observe",
        "no_action": {
          "action_id": "tls_a2",
          "yes_action": {
            "action_id": "tls_a5",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "tls_hsts_misconfigured_tls_e3",
        "action_id": "tls_a6",
        "yes_action": "observe",
        "no_action": {
          "action_id": "tls_a2",
          "yes_action": {
            "action_id": "tls_a7",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "tls_http_to_https_misconfigured_tls_e4",
        "action_id": "tls_a8",
        "yes_action": "observe",
        "no_action": {
          "action_id": "tls_a2",
          "yes_action": {
            "action_id": "tls_a9",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "tls_secure_cookies_misconfigured_tls_e5",
        "action_id": "tls_a10",
        "yes_action": "observe",
        "no_action": {
          "action_id": "tls_a2",
          "yes_action": {
            "action_id": "tls_a11",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "tls_certificate_pinning_misconfigured_tls_e6",
        "action_id": "tls_a12",
        "yes_action": "observe",
        "no_action": {
          "action_id": "tls_a2",
          "yes_action": {
            "action_id": "tls_a13",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "tls_terminator_unavailable_tls_e7",
        "action_id": "tls_a2",
        "yes_action": "observe",
        "no_action": "notify"
      },
      {
        "name": "tls_endpoint_unavailable_tls_e8",
        "action_id": "tls_a14",
        "yes_action": "observe",
        "no_action": "notify"
      }
    ]
  },
  "chef_recipes" : [
      {
        "name": "tls_r1",
        "recipe_description": "Install TLS Prober.",
        "associated_metrics": [],
        "associated_measurements": [],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r2",
        "recipe_description": "Install TLS Terminator.",
        "associated_metrics": [
          "tls_crypto_strength_m3",
          "forward_secrecy_m4",
          "hsts_m5",
          "https_redirect_m6",
          "secure_cookies_m7",
          "ceritificate_pinning_m10"
        ],
        "associated_measurements": [
          "tls_crypto_strength_level_msr1",
          "tls_forward_secrecy_msr2",
          "tls_hsts_msr3",
          "tls_http_to_https_redirect_msr4",
          "tls_force_secure_cookies_msr5",
          "tls_certificate_pinning_msr6",
          "tls_terminator_availability_msr7"
        ],
        "dependent_components": [
          "tls_terminator"
        ]
      },
      {
        "name": "tls_r3",
        "recipe_description": "Install TLS Terminator Configurator.",
        "associated_metrics": [],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator"
        ]
      },
      {
        "name": "tls_r4",
        "recipe_description": "Install TLS terminator Controller.",
        "associated_metrics": [],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_configurator",
          "tls_terminator"
        ]
      },
      {
        "name": "tls_r5",
        "recipe_description": "Reconfigure TLS crypto strength to M3_value.",
        "associated_metrics": [
          "tls_crypto_strength_m3"
        ],
        "associated_measurements": [
          "tls_crypto_strength_level_msr1"
        ],
        "dependent_components": [
          "tls_terminator"
        ]
      },
      {
        "name": "tls_r6",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr1 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_crypto_strength_level_msr1"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r7",
        "recipe_description": "Restart TLS Terminator.",
        "associated_metrics": [],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_controller"
        ]
      },
      {
        "name": "tls_r8",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr7 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_terminator_availability_msr7"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r9",
        "recipe_description": "Reconfigure TLS_forward_secrecy to specs:forward_secrecy:M4 value.",
        "associated_metrics": [
          "forward_secrecy_m4"
        ],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_configurator"
        ]
      },
      {
        "name": "tls_r10",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr2 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_forward_secrecy_msr2"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r11",
        "recipe_description": "Reconfigure TLS_hsts to specs:hsts:M5 value.",
        "associated_metrics": [
          "hsts_m5"
        ],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_configurator"
        ]
      },
      {
        "name": "tls_r12",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr3 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_hsts_msr3"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r13",
        "recipe_description": "Reconfigure TLS_http_to_https to specs:http_to_https:M6 value.",
        "associated_metrics": [
          "https_redirect_m6"
        ],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_configurator"
        ]
      },
      {
        "name": "tls_r14",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr4 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_http_to_https_redirect_msr4"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r15",
        "recipe_description": "Reconfigure TLS_secure_cookies to specs:secure_cookies:M7 value.",
        "associated_metrics": [
          "secure_cookies_m7"
        ],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_configurator"
        ]
      },
      {
        "name": "tls_r16",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr5 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_force_secure_cookies_msr5"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r17",
        "recipe_description": "Reconfigure TLS_certificate_pinning to specs:certificate_pinning:M10 value.",
        "associated_metrics": [
          "ceritificate_pinning_m10"
        ],
        "associated_measurements": [],
        "dependent_components": [
          "tls_terminator_configurator"
        ]
      },
      {
        "name": "tls_r18",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr6 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_certificate_pinning_msr6"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      },
      {
        "name": "tls_r19",
        "recipe_description": "Request, to an external service, Endpoint restart.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_endpoint_availability_msr8"
        ],
        "dependent_components": [
          "tls_terminator_controller"
        ]
      },
      {
        "name": "tls_r20",
        "recipe_description": "Invoke TLS Prober to take measurement tls-msr8 and label the event as remediation-event.",
        "associated_metrics": [],
        "associated_measurements": [
          "tls_endpoint_availability_msr8"
        ],
        "dependent_components": [
          "tls_prober"
        ]
      }
    ]
}
