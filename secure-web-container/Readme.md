# Secure Web Container README #

## SPECS Application Description ##
Secure Web Container 

### Templates Available ###
SPECS Secure Web Container offer two templates, one representing security controls according to CSA Cloud Control Matrix, the second representing controls according to NIST Control Framework (NIST SP800-53) 

### Mechanisms supported ###
Secure Web Container supports the following mechanisms:
* Web Pool (mandatory mechanism)
* Software Vulnerability Assessment (SVA)
* Denial of Service Detection and Mitigation (DoSProtection)
* TLS 
