# README #
SPECS App Repository contains all the configuration file for the default applications developed using the SPECS Framework
 
### SPECS Application Description ###
Each Application have a dedicated directory that contains:
* A "templates" folder, that collects the Ws-Agreement templates used for the negotiation process
* A "mechanisms" folder, that collects the mechanisms metadata used for offering security mechanisms negotiated by SPECS application
* A Readme file, that briefly summarize the application behaviour

### How do I get set up a SPECS Application? ###
In order to start a new SPECS application use the SPECS Platform Interface.
