# AAAaaS README #

## SPECS Application Description ##
End-to-End Encryption and AAA as a service

### Templates Available ###
SPECS AAAaaS offer two templates, one representing security controls according to CSA Cloud Control Matrix, the second representing controls according to NIST Control Framework (NIST SP800-53) 

### Mechanisms supported ###
SPECS AAAaaS supports the following mechanisms:
* DBaaS (mandatory mechanism)
* E2EE Vulnerability Assessment (SVA)
* AAA
