# E2EE README #

## SPECS Application Description ##
End-to-End Encryption 

### Templates Available ###
SPECS E2EE offer two templates, one representing security controls according to CSA Cloud Control Matrix, the second representing controls according to NIST Control Framework (NIST SP800-53) 

### Mechanisms supported ###
SPECS E2EE supports the following mechanisms:
* DBaaS (mandatory mechanism)
* E2EE Vulnerability Assessment (SVA)

 